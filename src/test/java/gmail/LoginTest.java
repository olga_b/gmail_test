package gmail;

import common.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.InboxPage;
import pages.LoginPage;
import pages.WelcomePage;


public class LoginTest extends BaseTest {
    private WebDriver driver = super.getDriver();

    public LoginTest(){
        super();
    }

    @Before
    public void initDriver(){

        driver.get(prop.getProperty("baseUrl"));
    }

    @Test
    public void login() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        WelcomePage welcomePage = loginPage.enterEmail(prop.getProperty("email"));
        InboxPage inboxPage = welcomePage.enterPassword(prop.getProperty("password"));


        Thread.sleep(1000);
    }

    @Test
    public void logout() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        WelcomePage welcomePage = loginPage.enterEmail(prop.getProperty("email"));
        InboxPage inboxPage = welcomePage.enterPassword(prop.getProperty("password"));
        inboxPage.logout();

        Thread.sleep(1000);
    }

    @After
    public void tearDown(){
        driver.close();
        driver.quit();
    }


}
