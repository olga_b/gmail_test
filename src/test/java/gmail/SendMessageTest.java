package gmail;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

public class SendMessageTest {

    private Properties prop = new Properties();
    private File userProp = new File("user.properties");
    private Message message;

    public SendMessageTest(){
    }

    @Before
    public void init(){
        InputStreamReader fileInput = null;
        try {
            fileInput = new InputStreamReader(new FileInputStream(userProp), "UTF-8");
            prop.load(fileInput);
        } catch (IOException e) {
            System.out.println(("Property file not found"));
        }
    }

    @Test
    public void sendMessage(){
        Gmail service = null;
        try {
            service = AutorizeGmailClientService.getGmailService("client_secret_18.json");

            MimeMessage mimeMessage = MessageCreator.createEmail(prop.getProperty("to"),
                    prop.getProperty("from"),prop.getProperty("subject"), prop.getProperty("bodyText"));
            message = MessageCreator.createMessageWithEmail(mimeMessage);
            message = service.users().messages().send(prop.getProperty("email"), message).execute();

            System.out.println("Message id: " + message.getId());

        } catch (IOException | MessagingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void receiveMessage(){
        try {
            Gmail service = AutorizeGmailClientService.getGmailService("client_secret_18.json");

            ListMessagesResponse response = service.users().messages().list(prop.getProperty("email"))
                    .setQ(prop.getProperty("query")).execute();

            List<Message> messages = new ArrayList<>();
            while (response.getMessages() != null) {
                messages.addAll(response.getMessages());
                if (response.getNextPageToken() != null) {
                    String pageToken = response.getNextPageToken();
                    response = service.users().messages().list("me")
                            .setQ(prop.getProperty("query"))
                            .setPageToken(pageToken).execute();
                } else {
                    break;
                }
            }

            assertThat(messages.size(),greaterThan(0));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
