package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public abstract class BaseTest {
    private WebDriver driver;
    protected Properties prop = new Properties();
    private File userProp = new File("user.properties");

    protected BaseTest(){
        initProperty();
        initDriver();
    }

    public WebDriver getDriver(){
        return driver;
    }

    private void initDriver(){
        System.setProperty("webdriver.chrome.driver", prop.getProperty("driver"));
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }
    protected void initProperty(){
        InputStreamReader fileInput = null;
        try {
            fileInput = new InputStreamReader(new FileInputStream(userProp), "UTF-8");
            prop.load(fileInput);
        } catch (IOException e) {
            System.out.println(("Property file not found"));
        }
    }
}
