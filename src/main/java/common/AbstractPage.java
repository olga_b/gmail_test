package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {
    private WebDriver driver;

    protected AbstractPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public WebDriver getDriver(){
        return driver;
    }

    public void waitUntil(ExpectedCondition condition){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(condition);
    }


}
