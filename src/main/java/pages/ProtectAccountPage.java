package pages;

import common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class ProtectAccountPage extends AbstractPage {

    @FindBy(xpath = "//span[text()='Готово']")
    private WebElement doneButton;

    public ProtectAccountPage(WebDriver driver) {
        super(driver);
    }

    public void clickNext(){
        waitUntil(ExpectedConditions.elementToBeClickable(doneButton));
        doneButton.click();
    }
}
