package pages;

import common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class InboxPage extends AbstractPage {

    @FindBy(xpath = "//a[contains(@href,'SignOutOptions')]")
    private WebElement accountButton;

    @FindBy(xpath = "//a[contains(@href,'Logout')]")
    private WebElement logoutButton;

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void logout(){
        waitUntil(ExpectedConditions.elementToBeClickable(accountButton));
        accountButton.click();

        waitUntil(ExpectedConditions.visibilityOf(logoutButton));
        logoutButton.click();
    }
}
