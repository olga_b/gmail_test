package pages;

import common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends AbstractPage {

    //@FindBy(xpath = "//input[contains(@type,'email')]")
    @FindBy(css = "input[type*='email']")
    private WebElement emailField;

    @FindBy(xpath = "//div[@id='identifierNext']")
    private WebElement nextButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public WelcomePage enterEmail(String email){
        waitUntil(ExpectedConditions.visibilityOf(emailField));
        emailField.sendKeys(email);
        nextButton.click();

        return new WelcomePage(getDriver());
    }


}
