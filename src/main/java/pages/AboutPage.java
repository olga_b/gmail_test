package pages;


import common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AboutPage extends AbstractPage{

    //@FindBy(xpath = "//a[contains(@data-g-label,'Sign in')]")
    @FindBy(css = "a[data-g-label*='Sign in']")
    private WebElement signInButton;

    public AboutPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickSingInButton(){
        signInButton.click();
        return new LoginPage(getDriver());
    }

}
