package pages;

import common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class WelcomePage extends AbstractPage {

    //@FindBy(css = "input[type='password']")
    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//div[@id='passwordNext']")
    private WebElement nextButton;

    public WelcomePage(WebDriver driver) {
        super(driver);
    }

    public InboxPage enterPassword(String password){
        waitUntil(ExpectedConditions.elementToBeClickable(passwordField));
        passwordField.sendKeys(password);
        nextButton.click();

        return new InboxPage(getDriver());

    }
}
